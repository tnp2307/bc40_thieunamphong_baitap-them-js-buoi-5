// bài 1
function calculate1() {
  var name = document.getElementById("txt-name").value;
  var thuNhap = document.getElementById("txt-thu-nhap").value * 1;
  var nguoiPhuThuoc = document.getElementById("txt-phu-thuoc").value * 1;
  var thueThuNhap = 0;
  var thueSuat = 0;
  function tinhThueSuat(thuNhap) {
    if (thuNhap <= 60000000) {
      return 0.05;
    } else if (thuNhap <= 120000000) {
      return 0.1;
    } else if (thuNhap <= 210000000) {
      return 0.15;
    } else if (thuNhap <= 384000000) {
      return 0.2;
    } else if (thuNhap <= 624000000) {
      return 0.25;
    } else if (thuNhap <= 960000000) {
      return 0.3;
    } else {
      return 0.35;
    }
  }
  thueThuNhap =
    (thuNhap - 4000000 - nguoiPhuThuoc * 1600000) * tinhThueSuat(thuNhap);
  document.getElementById(
    "result1"
  ).innerHTML = `Thuế thu nhập cá nhân của ${name} là : ${thueThuNhap}`;
}
// bài 2
document.getElementById("type-dan").addEventListener("click", function () {
  document.getElementById("form-ket-noi").classList.add("d-none");
});
document
  .getElementById("type-doanh-nghiep")
  .addEventListener("click", function () {
    document.getElementById("form-ket-noi").classList.remove("d-none");
  });
function calculate2() {
  var khachHang = document.getElementById("txt-ma-khach-hang").value;
  var soKenh = document.getElementById("txt-kenh").value * 1;
  var soKetNoi = document.getElementById("txt-ket-noi").value * 1;
  var tienCap = 0;
  var loaiKhachHang = document.querySelector(
    'input[name="loai-khach-hang"]:checked'
  ).value;
  const phiXuLyHoaDonNhaDan = 4.5;
  const phiDichVuCoBanNhaDan = 20.5;
  const donGiaThueKenhCaoCapNhaDan = 7.5;
  const phiXuLyHoaDonDoanhNghiep = 15;
  const phiDichVuCoBanDoanhNghiep = 75;
  const donGiaThueKenhCaoCapDoanhNghiep = 50;
  function tinhTienCap(soKetNoi, soKenh, loaiKhachHang) {
    switch (loaiKhachHang) {
      case "doanhnghiep":
        if (soKetNoi <= 10) {
          return (
            phiXuLyHoaDonDoanhNghiep +
            phiDichVuCoBanDoanhNghiep +
            donGiaThueKenhCaoCapDoanhNghiep * soKenh
          );
        } else {
          return (
            phiXuLyHoaDonDoanhNghiep +
            phiDichVuCoBanDoanhNghiep +
            (soKetNoi - 10) * 5 +
            donGiaThueKenhCaoCapDoanhNghiep * soKenh
          );
        }
      case "nhadan":
        return (
          phiXuLyHoaDonNhaDan +
          phiDichVuCoBanNhaDan +
          donGiaThueKenhCaoCapNhaDan * soKenh
        );
        
    }
  }
  tienCap = tinhTienCap(soKetNoi, soKenh, loaiKhachHang);
  document.getElementById(
    "result2"
  ).innerHTML = `Tiền cáp của ${khachHang} là : ${tienCap}`;
}
